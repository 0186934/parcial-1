var mysql_host = "localhost";
var Sequelize = require('sequelize');
var sequelize = new Sequelize('AuricDB', 'root', 'Tokyo.moon1',{
	host: mysql_host,
	dialect: 'mysql'
});


var photoevent = sequelize.define('photoevents', {
	"id"				: { type: Sequelize.INTEGER, primaryKey: true},
	"place"				: { type: Sequelize.STRING },
	"location"			: { type: Sequelize.JSON },
	"time"			    : { type: Sequelize.DATE },
	"date_compromise"	: { type: Sequelize.DATE },
	"date_created"		: { type: Sequelize.DATE },
	"description"		: { type: Sequelize.STRING },
	"status"			: { type: Sequelize.STRING },
	"finance_aprov"		: { type: Sequelize.BOOLEAN },
    "event_type"		: { type: Sequelize.STRING }

});

var event_type = sequelize.define('events_types', {
	"id"				: { type: Sequelize.INTEGER, primaryKey: true},
	"name"				: { type: Sequelize.STRING },
	"description"		: { type: Sequelize.STRING },
});


var transaction = sequelize.define('transactions', {
	"id"				: { type: Sequelize.INTEGER, primaryKey: true},
	"date_made"			: { type: Sequelize.DATE },
	"approved_num"		: { type: Sequelize.STRING },
	"quantity"		    : { type: Sequelize.FLOAT }
});



photoevent.sync();
events_type.sync();
transaction.sync();
module.exports = photoevent;
module.exports = events_type;
module.exports = transaction;