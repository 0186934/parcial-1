var mysql_host = "localhost";
var Sequelize = require('sequelize');
var sequelize = new Sequelize('AuricDB', 'root', 'Tokyo.moon1',{
	host: mysql_host,
	dialect: 'mysql'
});


var photographer = sequelize.define('photographers',{
	//"id"				: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement : true},
	"first_name"		: { type: Sequelize.STRING },
	"last_name"			: { type: Sequelize.STRING },
	"age"				: { type: Sequelize.INTEGER},
	"email"             : { type: Sequelize.STRING },
	"adress1"           : { type: Sequelize.STRING },
	"adress2"           : { type: Sequelize.STRING },
	"state"				: { type: Sequelize.STRING },
	"city"				: { type: Sequelize.STRING },
	"phone_number"		: { type: Sequelize.INTEGER },
	"card_number"		: { type: Sequelize.JSON },
	"portfolio"			: { type: Sequelize.JSON },
	"description"		: { type: Sequelize.STRING },
	"status"			: { type: Sequelize.STRING }

});

photographer .sync();
module.exports = photographer  ;