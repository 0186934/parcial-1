var mysql_host = "localhost";
var Sequelize = require('sequelize');
var sequelize = new Sequelize('AuricDB', 'root', 'Tokyo.moon1',{
	host: mysql_host,
	dialect: 'mysql'
});



var portfolio = sequelize.define('portfolios', {
	"id"				: { type: Sequelize.INTEGER, primaryKey: true},
	"fname"				: { type: Sequelize.STRING },
	"lname"				: { type: Sequelize.STRING },
	"phone"				: { type: Sequelize.STRING },
	"email"				: { type: Sequelize.STRING },
	"description"		: { type: Sequelize.STRING },
	"username"			: { type: Sequelize.STRING },
	"location"			: { type: Sequelize.JSON },
	"photo"				: { type: Sequelize.JSON}
});

portfolio.sync();
module.exports = portfolio;
