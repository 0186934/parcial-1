var express = require('express');
var router = express.Router();
var eventTable = require("./models/event");
var userTable = require("./models/user");

router.get('/user/:id', function(req, res, next) {
	var users;
	if(req.params.id){
		userTable.findOne({
			where: {
		        id : id
	      	}
      	}).then(function(users){            
            res.json(users);
        });		
	}
	else{
		userTable.findAll().then(function(users) {            
            res.json( users );
        });				
	}	

});

router.post('/user', function(req, res, next) {  
	var user = {			
		
	}
	for (var column in req.body) {
		user[column] = req.body[column];
    }
	userTable.build(user).save().catch(function (err) {
        console.log(err);
    });
});


router.put('/user/:id', function(req, res, next) {
	var id = req.params.id;
	var user = { }

	for (var column in req.body) {
		user[column] = req.body[column];
    }

	userTable.update(user, {
      where: {
        id : id
      }
    }).catch(function (err) {
        console.log("Caught error on job: " + err);
    });
});


router.delete('/user/:id', function(req, res, next) {
	var id = req.params.id;
	
	userTable.destroy({
	    where: {
	        id : id
	    }
	});
	
});


module.exports = router;