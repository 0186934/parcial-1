// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("btn btn-default")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

////////////////////////////////////////////////////////
var myNodelist = document.getElementsByName("Card");
var i;
for (i = 0; i < myNodelist.length; i++) {
  var span = document.createElement("SPAN");
  myNodelist[i].appendChild(span);
}

var myNodelist2 = document.getElementsByName("Name");
var i2;
for (i2 = 0; i2 < myNodelist2.length; i2++) {
  var span = document.createElement("SPAN");
  myNodelist[i2].appendChild(span);
}

var myNodelist3 = document.getElementsByName("Date");
var i3;
for (i3 = 0; i3 < myNodelist3.length; i3++) {
  var span = document.createElement("SPAN");
  myNodelist[i3].appendChild(span);
}

// Create a new list item when clicking on the "Add" button
function newElement() {
  var li = document.createElement("li");
  var li2 = document.createElement("li");
  var li3 = document.createElement("li");
	
  var inputValue = document.getElementById("myInput").value;
  var inputValue2 = document.getElementById("myInput2").value;
  var inputValue3 = document.getElementById("myInput3").value;
  
  var t = document.createTextNode(inputValue);
  li.appendChild(t);
	
  var t2 = document.createTextNode(inputValue2);
  li2.appendChild(t2);
	
  var t3 = document.createTextNode(inputValue3);
  li3.appendChild(t3);

  if (inputValue === ''||inputValue2 === ''||inputValue3 === '') {
    alert("Complete the information, please!");
  } else {
    document.getElementById("myUL").appendChild(li);
	document.getElementById("myUL2").appendChild(li2);
	document.getElementById("myUL3").appendChild(li3);

  }
  document.getElementById("myInput").value = "";
  document.getElementById("myInput2").value = "";
  document.getElementById("myInput3").value = "";
 
}

function mostrar() {
    document.getElementById("sidebar").style.width = "300px";
    document.getElementById("contenido").style.marginLeft = "300px";
    document.getElementById("abrir").style.display = "none";
    document.getElementById("cerrar").style.display = "inline";
}

function ocultar() {
    document.getElementById("sidebar").style.width = "0";
    document.getElementById("contenido").style.marginLeft = "0";
    document.getElementById("abrir").style.display = "inline";
    document.getElementById("cerrar").style.display = "none";
}

// sandbox disable popups
    if (window.self !== window.top && window.name!="view1") {;
      window.alert = function(){/*disable alert*/};
      window.confirm = function(){/*disable confirm*/};
      window.prompt = function(){/*disable prompt*/};
      window.open = function(){/*disable open*/};
    }
    
    // prevent href=# click jump
    document.addEventListener("DOMContentLoaded", function() {
      var links = document.getElementsByTagName("A");
      for(var i=0; i < links.length; i++) {
        if(links[i].href.indexOf('#')!=-1) {
          links[i].addEventListener("click", function(e) {
          console.debug("prevent href=# click");
              if (this.hash) {
                if (this.hash=="#") {
                  e.preventDefault();
                  return false;
                }
                else {
                  /*
                  var el = document.getElementById(this.hash.replace(/#/, ""));
                  if (el) {
                    el.scrollIntoView(true);
                  }
                  */
                }
              }
              return false;
          })
        }
      }
    }, false);